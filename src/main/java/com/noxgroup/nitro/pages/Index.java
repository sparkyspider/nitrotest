package com.noxgroup.nitro.pages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class Index {

    private static String VERSION = "0";
    private static String HOST_PROVIDER = "Rimuhosting";

    @RequestMapping(method = RequestMethod.GET)
    public String index(HttpServletRequest request, Model model) {
        model.addAttribute("server", request.getServerName());
        model.addAttribute("port", request.getServerPort());
        model.addAttribute("version", VERSION);
        model.addAttribute("host", HOST_PROVIDER);

        return "index";
    }


}




