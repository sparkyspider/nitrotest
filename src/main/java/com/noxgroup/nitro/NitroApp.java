package com.noxgroup.nitro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableAsync
@EnableScheduling
/**
 * @Author: Mark van Wyk
 */
public class NitroApp extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(NitroApp.class);

    public static void main (String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(NitroApp.class, args);
    }

}